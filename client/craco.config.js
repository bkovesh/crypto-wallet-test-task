const { ESLINT_MODES } = require("@craco/craco");
module.exports = {
  eslint: {
    enable: false,
    configure: {
      rules: {
        "no-unused-vars": "off"
      }
    }
  }
};