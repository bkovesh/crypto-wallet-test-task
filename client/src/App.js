import React, {useState, useEffect} from 'react';
import {BrowserRouter as Router, Switch, Link, Route, Redirect} from 'react-router-dom';
import * as Components from './components';
import ImageLogo from './assets/images/logo.png';
import config from './config';

const style = {
  h1: {
    textDecoration: 'none',
  },
  logo: {
    width: 100,
    height: 100,
    margin: 20,
  },
}

function App() {
  // const isLogged = localStorage.getItem('encryptedMnemonic')
  // console.log('isLogged',isLogged,localStorage.getItem('encryptedMnemonic'))
  const [mnemonic, setMnemonic] = useState();
  const [isLogged, setIsLogged] = useState(localStorage.getItem('encryptedMnemonic'));

  useEffect(() => {
    run();
  },[])

  const run = async () => {
    try {
      const response = await fetch(`${config.urlServer}/fill-db`,{
        method:'GET',
      });
      const result = await response.json();
      console.log('run', result);
      setIsLogged(localStorage.getItem('encryptedMnemonic'))
    } catch (e) {
      console.error(e);
    }
  }


  const downloadMnemonic = () => {
    const element = document.createElement("a");
    const file = new Blob([mnemonic], {type: 'text/plain'});
    element.href = URL.createObjectURL(file);
    element.download = "Bitgesell.txt";
    document.body.appendChild(element); // Required for this to work in FireFox
    element.click();
  }

  const exit = async () => {
    try {
      localStorage.setItem('encryptedMnemonic',null)
      localStorage.setItem('pin',null)
    } catch (e) {
      console.error(e);
    }
  }

  return (
  <Router>
    <div className="App">
      <div className="AppContainer">


        <Components.Menu>
          <div style={{margin: 30}}></div>
          <Link
          to={`/wallet`}
          style={{
            textDecoration: 'none',
            color: '#fff',
          }}
          >
            <div
            style={{
              cursor: 'pointer',
              padding:10
            }}
            >
              Wallet
            </div>
          </Link>
          <Link
          to={`/history`}
          style={{
            textDecoration: 'none',
            color: '#fff',
          }}
          >
            <div
            style={{
              cursor: 'pointer',
              padding:10
            }}
            >
              History
            </div>
          </Link>
          <div
          style={{
            cursor: 'pointer',
            padding:10
          }}
          onClick={downloadMnemonic}
          >
            Backup
          </div>
          <Link
          to={`/`}
          style={{
            textDecoration: 'none',
            color: '#fff',
          }}
          >
            <div
            style={{
              cursor: 'pointer',
              padding:10
            }}
            onClick={exit}
            >
              Exit
            </div>
          </Link>
        </Components.Menu>



        <Link
        to="/"
        style={style.h1}
        >
          <img
          src={ImageLogo}
          style={style.logo}
          alt="Bitgesell"
          />
        </Link>

        <Switch>
          <Route exact path="/">
            <Components.PageSign/>
          </Route>
          <Route exact path="/login">
            <Components.PageLogin/>
          </Route>
          <Route exact path="/reg">
            <Components.PageReg/>
          </Route>
          <Route exact path="/pin">
            <Components.PagePin/>
          </Route>
          <Route exact path="/mnemonic">
            <Components.PageEnterMnemonic/>
          </Route>
          <Route exact path="/from-file">
            <Components.PageSignFromFile/>
          </Route>
          <Route exact path="/wallet">
            { !(!isLogged || isLogged==='null') ? <Components.PageWallet/> : <Redirect to="/"/> }
            {/*<Components.PageWallet/>*/}
          </Route>
          <Route exact path="/history">
            { !(!isLogged || isLogged==='null') ? <Components.PageHistory/> : <Redirect to="/"/> }
            {/*<Components.PageHistory/>*/}
          </Route>
        </Switch>

      </div>
    </div>
  </Router>
  );
}

export default App;
