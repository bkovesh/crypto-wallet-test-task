import React, {Fragment, useState} from 'react';


function PageWallet(props) {
  const { children } = props;
  const [openMenu, setOpenMenu] = useState(false);

  return (
    <Fragment>

      <div
      style={{
        position: 'fixed',
        top:0,left:0,
        padding: 20,
        cursor: 'pointer',
      }}
      onClick={() => setOpenMenu(true)}
      >
        <div
        >
          Menu
        </div>
      </div>


      { openMenu &&
      <div
      style={{
        position: 'fixed',
        top:0,left:0,
        width:200,
        backgroundColor:'rgba(40,39,46,0.8)',
        padding: 10,
        height:'100vh',
      }}
      >
        <div
        style={{
          cursor: 'pointer',
          padding:10,
          textAlign:'left',
        }}
        onClick={() => setOpenMenu(false)}
        >
          Close
        </div>

        {children}

      </div>
      }

    </Fragment>
  );
}

export default PageWallet;
