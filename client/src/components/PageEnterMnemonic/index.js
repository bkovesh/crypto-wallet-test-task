import React, {useState, useEffect} from 'react';
import {BrowserRouter as Router, Switch, Route, Link, useRouteMatch} from 'react-router-dom';
import * as Components from '../../components';
import s from './style.module.sass';
import * as Actions from "../../actions";
const Crypto = new Actions.Crypto();


function PageEnterMnemonic() {
  let { path, url } = useRouteMatch();
  const [pin, setPin] = useState('');
  const [mnemonic, setMnemonic] = useState('');
  const [isCorrect, setIsCorrect] = useState(false);


  useEffect(()=>{
    run()
  },[pin,mnemonic])


  const run = async (e) =>  {
    const encrypted = await Crypto.encrypt(pin,mnemonic)
    localStorage.setItem('encryptedMnemonic',encrypted)
    localStorage.setItem('pin',pin)
  }


  const handleSetMnemonic = async (e) =>  {
    setMnemonic(e.target.value)
  }


  const handleSetPin = (e) =>  {
    setPin(e.target.value)
  }


  return (
    <div className={s.container}>


      <div>
        <div className={s.text}>
          Please, enter your PIN
        </div>

        <div>
          <input
          className={s.input}
          placeholder="mnemonic"
          // type="password"
          onChange={handleSetMnemonic}
          />
        </div>

        <div>
          <input
          className={s.input}
          placeholder="pin"
          type="password"
          onChange={handleSetPin}
          />
        </div>
      </div>


      <div>
        <Link
        className={s.link}
        to="wallet"
        >
          <div className={s.btnBig}>
            Continue
          </div>
        </Link>
      </div>

    </div>
  );
}

export default PageEnterMnemonic;
