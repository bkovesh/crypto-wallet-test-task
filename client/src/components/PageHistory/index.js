import React, {useEffect, useState} from 'react';
import {BrowserRouter as Router, Switch, Route, Link, useRouteMatch} from 'react-router-dom';
import moment from 'moment';
import * as Components from '../../components';
import * as Actions from '../../actions';
import s from './style.module.sass';
import IconLink from '../../assets/icons/link.svg';
import config from "../../config";
const BTC = new Actions.BTC('testnet');
const Crypto = new Actions.Crypto();


const style = {
  table: {
    borderCollapse: 'collapse'
  },
  th: {
    borderBottom: '1px solid lightgrey',
    padding:10,
  },
  tdBold: {
    fontWeight:'bold',
    padding:10,
  },
  tx: {
    padding:10,
  },
  toAddress: {
    padding:5,
  },
  tac: {
    textAlign:'center',
  },
  link: {
    textDecoration:'none',
    color:'#fff',
  },
  hr: ({color='lightgrey',height=10}) => { return {
    width: '100%',
    height: 1,
    backgroundColor: color,
    margin: height/2,
  }},
}



function PageHistory() {
  const [isCopied, setIsCopied] = useState(false);
  const [mnemonic, setMnemonic] = useState();
  const [address, setAddress] = useState('');
  const [receiver, setReceiver] = useState('');
  const [amount, setAmount] = useState(0);
  const [balance, setBalance] = useState();
  const [history, setHistory] = useState();
  const [txId, setTxId] = useState('');
  history && history.sort((a,b) => b.time-a.time)

  useEffect(()=>{
    // getSeedFromLocalStorage()
    run()
  },[])



  const run = async () => {
    try {
      console.log('run');
      const response = await fetch(`${config.urlServer}/get-all-txs`,{
        method:'GET',
      });
      const result = await response.json();
      console.log('run', result);
      if (response.ok) setHistory(result.data)
    } catch (e) {
      console.error(e);
    }
  }


  const send = async () => {
    const rawTx = await BTC.signRawTx({
      mnemonic,
      amount,
      addressTo: receiver,
    })
    const result = await BTC.sendRawTx({rawTx})
    setTxId(result.result)
    console.log('send',result)
  }


  return (
    <div className={s.container}>


      <h2>
        Operations
      </h2>


      <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems:'center'
      }}
      >
        <input
        className={s.input}
        type="text"
        placeholder="Receiver"
        value={receiver}
        onChange={(e) => setReceiver(e.target.value)}
        />
        <input
        className={s.input}
        type="text"
        placeholder="Amount"
        value={amount}
        onChange={(e) => setAmount(e.target.value)}
        />
      </div>
      <div
      className={s.btnBig}
      onClick={send}
      >
        Send
      </div>

      <div style={style.hr({color:'transparent',height:10})}></div>

      <h2>
        History
      </h2>

      <div>
        { history && history.map((tx,it) => {
          let {sender,receiver,amount,fee, time} = tx;
          // let addressFrom = tx.scriptPubKey.addresses[0]
          // let isIncoming = direction==='in'
          return (
          <div
          key={`tx-${it}`}
          className={s.tx}
          >
            <div style={{marginRight:20,fontWeight:500}}>
              {amount}
            </div>

            <div style={{marginRight:20,fontWeight:500}}>
              <span style={{color:'#585858',fontWeight:500}}>from:{' '}</span>
              {sender}
            </div>

            <div style={{marginRight:20,fontWeight:500}}>
              <span style={{color:'#585858',fontWeight:500}}>to:{' '}</span>
              {receiver}
            </div>

            <div style={{marginRight:20,color:'#585858',fontWeight:500}}>
              {moment.unix(new Date(time)).fromNow()}
            </div>

            <div style={{marginRight:20,fontWeight:500}}>
              <span style={{color:'#585858',fontWeight:500}}>fee:{' '}</span>
              {fee}
            </div>

            {/*{ confirmations &&*/}
            {/*<div>*/}
            {/*  Confirmations:{' '}{confirmations}*/}
            {/*</div>*/}
            {/*}*/}

            {/*<div style={{...style.tac,...{}}}>*/}
            {/*  <a*/}
            {/*  style={style.link}*/}
            {/*  href={`https://btc.bitaps.com/${txid}`}*/}
            {/*  target="_blank"*/}
            {/*  rel="noopener noreferrer"*/}
            {/*  >*/}
            {/*    <img*/}
            {/*    style={{width:20,height:20}}*/}
            {/*    src={IconLink}*/}
            {/*    alt="Show"*/}
            {/*    />*/}
            {/*  </a>*/}
            {/*</div>*/}
          </div>
          )
        })}
        { history && history.length===0 && 'You have no transactions'}
      </div>

      <div style={style.hr({color:'transparent',height:80})}></div>

    </div>
  );
}

export default PageHistory;
