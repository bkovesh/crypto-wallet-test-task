import React from 'react';
import {BrowserRouter as Router, Switch, Route, Link, useRouteMatch} from 'react-router-dom';
import * as Components from '../../components';
import s from './style.module.sass';

function PageLogin() {
  let { path, url } = useRouteMatch();
  return (
    <div className={s.container}>
      <h2>
        Sign in
      </h2>


      <Switch>
        <Route exact path={`${path}/`}>
          <Components.FormLogin/>
        </Route>
      </Switch>
    </div>
  );
}

export default PageLogin;
