import React, {useState, useEffect} from 'react';
import {BrowserRouter as Router, Switch, Route, Link, useRouteMatch} from 'react-router-dom';
import * as Components from '../../components';
import s from './style.module.sass';
import * as Actions from '../../actions';
const BTC = new Actions.BTC('testnet');
const Crypto = new Actions.Crypto();

function PageReg() {
  let { path, url } = useRouteMatch();
  const [pin, setPin] = useState('');
  const [isCorrect, setIsCorrect] = useState(false);

  useEffect(()=>{
    run()
  },[pin])

  const run = async (e) =>  {
    const encryptedMnemonic = localStorage.getItem('encryptedMnemonic')
    console.log(encryptedMnemonic)
    if (!encryptedMnemonic || encryptedMnemonic==='null') return;
    if (!pin) return;
    const mnemonic = await Crypto.decrypt(String(pin),encryptedMnemonic)
    console.log(mnemonic)
    setIsCorrect(BTC.validateMnemonic(mnemonic))
  }

  const handleSetPin = (e) =>  {
    setPin(e.target.value)
  }

  return (
    <div className={s.container}>


      <div>
        <div className={s.text}>
          Please, enter your PIN
        </div>

        <input
        className={s.input}
        placeholder="_ _ _ _"
        type="password"
        onChange={handleSetPin}
        />
      </div>


      {isCorrect &&
      <div>
        <Link
        className={s.link}
        to="wallet"
        >
          <div className={s.btnBig}>
            Continue
          </div>
        </Link>
      </div>
      }

    </div>
  );
}

export default PageReg;
