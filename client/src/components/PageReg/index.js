import React, {useEffect, useState} from 'react';
import {BrowserRouter as Router, Switch, Route, Link, useHistory} from 'react-router-dom';
import * as Components from '../../components';
import * as Actions from '../../actions';
import s from './style.module.sass';
const BTC = new Actions.BTC();
const Crypto = new Actions.Crypto();

function PageReg() {
  let history = useHistory();
  const [mnemonic, setMnemonic] = useState();
  const [isCopied, setIsCopied] = useState(false);
  const [pin, setPin] = useState('');

  useEffect(()=>{
    getMnemonic()
    run()
  },[])

  const run = async () => {
    const mnemonic = await BTC.genMnemonic()
    console.log("genMnemonic",mnemonic)
    const seed = await BTC.mnemonicToSeed(mnemonic)
    console.log("mnemonicToSeed",seed)
    const node = await BTC.nodeFromMnemonic(mnemonic)
    console.log("nodeFromMnemonic",node)
    const address = await BTC.genAddressFromNode(node)
    console.log("genAddressFromNode",address)
  }

  const getMnemonic = async () => {
    const mnemonic = await BTC.genMnemonic()
    setMnemonic(mnemonic)
  }

  const enter = async () => {
    if (!pin) return;
    const encrypted = await Crypto.encrypt(pin,mnemonic)
    await localStorage.setItem('encryptedMnemonic',encrypted)
    console.log('enter',encrypted)
    history.push('/wallet')
  }

  const handleSetPin = async (e) => {
    setPin(e.target.value)
  }

  return (
    <div className={s.container}>

      <div className={s.m30}>
        <div className={s.text}>
          Your seed phrase. Please, save it somewhere and keep in secret.
        </div>
        <div
        className={s.seed}
        onClick={() => {
          navigator.clipboard.writeText(mnemonic)
          setIsCopied(true)
          setTimeout(() => setIsCopied(false),1000)
        }}
        >
          {!isCopied ? mnemonic : 'Copied!'}
        </div>
      </div>

      <div>
        <div className={s.text}>
          Please, enter your PIN
        </div>

        <input
        className={s.input}
        placeholder="_ _ _ _"
        type="password"
        value={pin}
        onChange={handleSetPin}
        />
      </div>

      <div>
        <div
        className={s.btnBig}
        onClick={enter}
        >
          Continue
        </div>
        {/*<Link*/}
        {/*className={s.link}*/}
        {/*to="wallet"*/}
        {/*>*/}
        {/*  <div*/}
        {/*  className={s.btnBig}*/}
        {/*  onClick={enter}*/}
        {/*  >*/}
        {/*    Continue*/}
        {/*  </div>*/}
        {/*</Link>*/}
      </div>

    </div>
  );
}

export default PageReg;
