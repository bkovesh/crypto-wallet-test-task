import React, {useState, useEffect} from 'react';
import {BrowserRouter as Router, Switch, Route, Link, useRouteMatch} from 'react-router-dom';
import * as Components from '../../components';
import s from './style.module.sass';
import * as Actions from "../../actions";
const Crypto = new Actions.Crypto();


function PageSignFromFile() {
  let { path, url } = useRouteMatch();
  const [pin, setPin] = useState('');
  const [mnemonic, setMnemonic] = useState('');
  const [isCorrect, setIsCorrect] = useState(false);


  useEffect(()=>{
    run()
  },[pin,mnemonic])


  const run = async (e) =>  {
    const encrypted = await Crypto.encrypt(pin,mnemonic)
    localStorage.setItem('encryptedMnemonic',encrypted)
    localStorage.setItem('pin',pin)
  }



  const handleSetPin = (e) =>  {
    setPin(e.target.value)
  }


  const handleUpload = async (e) => {
    const reader = new FileReader()
    reader.onabort = () => console.log('file reading was aborted')
    reader.onerror = () => console.log('file reading has failed')
    reader.onload = () => {
      try {
        const binaryStr = reader.result
        var bufView = new Uint8Array(binaryStr);
        var enc = new TextDecoder("utf-8");
        const string = enc.decode(bufView)
        console.log(enc.decode(bufView))
        setMnemonic(string)
      } catch (e) {
        console.error(e);
      }
    }
    reader.readAsArrayBuffer(e.target.files[0])
  }

  return (
    <div className={s.container}>



      <div>
        <label
        htmlFor="file"
        className={s.btnBig}
        style={{cursor:'pointer',margin:10}}
        >
          Choose file
        </label>
        <input
        style={{display:'none'}}
        id="file"
        type="file"
        name="file"
        onChange={handleUpload}
        />
      </div>


      <div>
        <div className={s.text}>
          Please, enter your PIN
        </div>

        <div>
          <input
          className={s.input}
          placeholder="pin"
          type="password"
          onChange={handleSetPin}
          />
        </div>
      </div>


      <div>
        <Link
        className={s.link}
        to="wallet"
        >
          <div className={s.btnBig}>
            Continue
          </div>
        </Link>
      </div>

    </div>
  );
}

export default PageSignFromFile;
