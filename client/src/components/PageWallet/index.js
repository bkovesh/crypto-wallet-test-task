import React, {useEffect, useState} from 'react';
import {BrowserRouter as Router, Switch, Route, Link, useRouteMatch} from 'react-router-dom';
import moment from 'moment';
import * as Components from '../../components';
import * as Actions from '../../actions';
import s from './style.module.sass';
import IconLink from '../../assets/icons/link.svg';
const BTC = new Actions.BTC('testnet');
const Crypto = new Actions.Crypto();


const style = {
  table: {
    borderCollapse: 'collapse'
  },
  th: {
    borderBottom: '1px solid lightgrey',
    padding:10,
  },
  tdBold: {
    fontWeight:'bold',
    padding:10,
  },
  tx: {
    padding:10,
  },
  toAddress: {
    padding:5,
  },
  tac: {
    textAlign:'center',
  },
  link: {
    textDecoration:'none',
    color:'#fff',
  },
  hr: ({color='lightgrey',height=10}) => { return {
    width: '100%',
    height: 1,
    backgroundColor: color,
    margin: height/2,
  }},
}


const getFromLocalStorage = (key) => {
  const item = localStorage.getItem(key)
  return JSON.parse(item)
}

const setToLocalStorage = (key,data) => {
  localStorage.setItem(key,JSON.stringify(data))
}


function PageWallet() {

  const [isCopied, setIsCopied] = useState(false);
  const [mnemonic, setMnemonic] = useState();
  const [address, setAddress] = useState('');
  let [wallets, setWallets] = useState(getFromLocalStorage('wallets'));
  const [receiver, setReceiver] = useState('');
  const [amount, setAmount] = useState(0);
  const [balance, setBalance] = useState();
  const [history, setHistory] = useState();
  const [txId, setTxId] = useState('');
  history && history.sort((a,b) => b.time-a.time)

  useEffect(()=>{
    // getSeedFromLocalStorage()
    // run()
  },[])

  useEffect(()=>{
    // mnemonic && getAddressFromMnemonic()
  },[mnemonic])

  useEffect(()=>{
    // if (!address) return
    // getBalance()
    // getHistory()
  },[address])

  const run = async () => {
    // const hdNode = BTC.hdNodeFromSeed()
    // const mnemonic = await BTC.privateKeyToMnemonic({
    //   privateKey: '00779d2e852d627595db1860086e8c59c35e5e0f11b2ca60bf209f1365c0d0f8'
    // })
    // console.log('privateKeyToMnemonic',mnemonic)
  }

  const getSeedFromLocalStorage = async () => {
    const encryptedMnemonic = localStorage.getItem('encryptedMnemonic')
    const pin = localStorage.getItem('pin')
    console.log('encryptedMnemonic',encryptedMnemonic,pin)
    const decrypted = await Crypto.decrypt(pin,encryptedMnemonic)
    const mnemonic = decrypted.toString()
    console.log('getSeedFromLocalStorage',mnemonic)
    setMnemonic(mnemonic)
  }

  const getAddressFromMnemonic = async () => {
    const address = await BTC.mnemonicToAddress({mnemonic})
    console.log('getAddressFromMnemonic',mnemonic,address)
    setAddress(address)
  }

  const getBalance = async () => {
    const result = await BTC.getBalance({
      // address:'bgl1qqgu9ct4guf9hqrntly7et20dym4nnytuc0tjq3',
      address,
    })
    console.log('getBalance',result)
    setBalance(result.data)
  }

  const getHistory = async () => {
    const result = await BTC.getHistory({
      // address:'bgl1qqgu9ct4guf9hqrntly7et20dym4nnytuc0tjq3',
      address,
    })
    console.log('getHistory',result)
    setHistory(result.data)
  }

  const genAddress = async () => {
    const result = await BTC.genAddress()
    console.log('genAddress',result)
    setReceiver(result)
  }

  const addNewAddress = async () => {
    const result = await BTC.genAddressFromMnemonic(mnemonic)
    console.log('addNewAddress',result)
    let wallets = getFromLocalStorage('wallets')
    if (!wallets) wallets = []
    wallets.push(result)
    setToLocalStorage('wallets',wallets)
    setWallets(wallets)
  }

  const removeAddress = async (address) => {
    let wallets = getFromLocalStorage('wallets')
    if (!wallets) wallets = []
    const index = wallets.indexOf(address);
    wallets.splice(index, 1);
    setToLocalStorage('wallets',wallets)
    setWallets(wallets)
  }

  const send = async () => {
    const rawTx = await BTC.signRawTx({
      mnemonic,
      amount,
      addressTo: receiver,
    })
    const result = await BTC.sendRawTx({rawTx})
    setTxId(result.result)
    console.log('send',result)
  }


  return (
    <div className={s.container}>


      <div
      style={{cursor:'pointer'}}
      onClick={() => {
        navigator.clipboard.writeText(address)
        setIsCopied(true)
        setTimeout(() => setIsCopied(false),1000)
      }}
      >
        {!isCopied ? address : 'Copied!'}
      </div>



      <div className={s.balance}>
        { balance!==undefined ? String(balance).slice(0,10) :
        <span>Loading balance...</span>
        }
        {' '}
        <span className={s.balanceCurrency}>BTC</span>
      </div>

      <div style={style.hr({color:'transparent',height:30})}></div>


      <h2>
        Wallets
      </h2>

      <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      }}
      >
        { wallets && wallets.map((wallet,iw) => {
          return (
          <div
          key={`wallet-${iw}`}
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            margin: 10,
          }}
          >
            <div
            style={{
              marginLeft: 10,
              marginRight: 10,
              cursor: 'pointer',
            }}
            >
              {wallet}
            </div>
            <div
            style={{
              marginLeft: 10,
              marginRight: 10,
              cursor: 'pointer',
            }}
            >
              Balance
            </div>
            <div
            style={{
              marginLeft: 10,
              marginRight: 10,
              cursor: 'pointer',
            }}
            onClick={removeAddress}
            >
              Remove
            </div>
          </div>
          )
        })}
        <div
        className={s.btnBig}
        onClick={addNewAddress}
        >
          Add new
        </div>
      </div>

      <div style={style.hr({color:'transparent',height:30})}></div>



    </div>
  );
}

export default PageWallet;
