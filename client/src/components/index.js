import PageSign from './PageSign';
import PageLogin from './PageLogin';
import PageReg from './PageReg';
import PagePin from './PagePin';
import PageWallet from './PageWallet';
import PageEnterMnemonic from './PageEnterMnemonic';
import PageSignFromFile from './PageSignFromFile';
import PageHistory from './PageHistory';

import FormLogin from './FormLogin';
import Menu from './Menu';


export {
  PageSign,
  PageLogin,
  PageReg,
  PagePin,
  PageWallet,
  PageEnterMnemonic,
  PageSignFromFile,
  PageHistory,

  FormLogin,
  Menu,
}