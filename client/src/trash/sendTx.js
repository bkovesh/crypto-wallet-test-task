
// address, from that comes tx: https://bgl.bitaps.com/bgl1qqgu9ct4guf9hqrntly7et20dym4nnytuc0tjq3
const signRawTxTest2 = async (params) => {
  const privateKey = "secret";
  const addressTo = 'bgl1q044q8wuww2flvmkhla06p653jrh447u46z2v9w';
  const txId = "c26297290c022f02f88386b175bd225dbfeb887bba5f5cba155fbe43c110afe0";
  const vOut = 0;
  await window.jsbtc.asyncInit();
  const { Transaction, Address, SIGHASH_ALL } = window.jsbtc;
  let a = new Address(privateKey);
  let addressFrom = a.address; // bgl1qqgu9ct4guf9hqrntly7et20dym4nnytuc0tjq3
  let tx = new Transaction({ lockTime: 32600, segwit: true });
  tx.addInput({ txId, vOut, address: addressFrom });
  tx.addOutput({ value: 49.9999 * 100000000, address: addressTo });
  tx.signInput(0, { privateKey, sigHashType: SIGHASH_ALL, value: 50 * 100000000});
  let rawTx = tx.serialize();
  return rawTx;
}
