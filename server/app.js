const createError = require('http-errors');
const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const http = require('http');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');
const indexRouter = require('./routes/index');
require('dotenv').config()

// ==========================================

const app = express();
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// ==========================================

const corsOptions = {
  allowedHeaders: ['Origin', 'X-Requested-With', 'Content-Type', 'Accept', 'Authorization'],
  // credentials: true,
  origin: (origin, callback) => {
    const whitelist = [
      config.urlClient,'http://localhost:5000'
    ];
    if (whitelist.indexOf(origin) !== -1 || !origin) {
      callback(null, true)
    } else {
      console.log(origin)
      callback(new Error("Not allowed by CORS",origin))
    }
  }
}
app.use(cors(corsOptions)) // insert always before routes

// ==========================================

app.use('/', indexRouter);

// ==========================================

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  return res.status(err.status || 500).send('error');
});

// ==========================================

const dbOpts = {
  autoIndex: false,
  useNewUrlParser: true,
  useUnifiedTopology: true
}

mongoose.connect(process.env.DB_URL, dbOpts, (err) => {
  if(err) return console.log(err);
  const port = process.env.PORT || 5000;
  const server = http.createServer(app);
  server.listen(port);
  server.on('error', (e) => console.log(e));
  server.on('listening', () => {
    console.log(`
    Server is listenting: http://localhost:${port}, 
    isDev:${config.isDev}, 
    client: ${config.urlClient}
    `)
  });
});


module.exports = app;
