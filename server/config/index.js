const isDev = process.env.NODE_ENV && process.env.NODE_ENV.trim()!=='production' ? true : false;


module.exports = {
  isDev,
  urlClient: isDev ? 'http://localhost:3000' : 'https://crypto-wallet-test-task.netlify.app',
}