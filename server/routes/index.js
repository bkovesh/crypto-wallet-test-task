const express = require('express');
const router = express.Router();
const Tx = require('../schemas/TxSchema');
const data = require('../data');


router.get('/', async (req, res, next) => {
  return res.status(200).send({ title: 'Express' });
});


router.get('/get-all-txs', async (req, res, next) => {
  try {
    let result = await Tx.find({});
    // console.log(result)
    return res.status(200).send({ data: result });
  } catch (e) {
    console.error(e);
    return res.status(500).send({ message: 'Server error' });
  }
});


router.get('/fill-db', async (req, res, next) => {
  try {
    let txs = data;
    await Tx.remove({});
    for (let i = 0; i < txs.length; i++) {
      let tx = txs[i];
      const newTx = new Tx(tx);
      await newTx.save()
    }
    return res.status(200).send({ message: 'Done', data: null });
  } catch (e) {
    console.error(e);
    return res.status(500).send({ message: 'Server error' });
  }
});


module.exports = router;
