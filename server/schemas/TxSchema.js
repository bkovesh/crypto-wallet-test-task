const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const TxSchema = new Schema({
  sender: {
    type: String,
    required: true,
  },
  receiver: {
    type: String,
    required: true,
  },
  time: {
    type: Date,
    default: new Date,
    required: true,
  },
  amount: {
    type: Number,
  },
  fee: {
    type: Number,
  },
});


module.exports = mongoose.model('Tx', TxSchema);